import React, { Component } from 'react';

import ProjectEntry from './ProjectEntry.class'

const yearsText = {
  1995: 'Mother buys a 486. Kid Santi experiments with MS Paint creating ski jump "games" where you moved a "sprite" down a slope and made it jump.',
  2000: 'Still at high-school, somehow I learn how to do simple ROMHacks (text and tiles).',
  2001: 'Somehow (?) I learned QBasic and starting doing some games.',
  2002: 'First year at college. Started doing graphical QBasic and DirectQB managing to impress my schoolmates. Then jumped into Java, maybe too early.',
  2003: 'The quest to create the Ultimate roguelike is on. Fruitless. Empirical gamedev knowledge, using java, continues building up.',
  2004: 'A young slashie seek to apply what he\'s learning about software engineering to game design',
  2005: 'Increased participation on USENET leads to the creation of Roguebasin, and the first 7DRL Challenge starts a series of "Completed" games',
  2006: 'The era of Java Roguelikes starts',
  2007: 'Fresh out of university. Temple of the Roguelike rises, and I start the ill-fated development of Pixal, my PBBG that never saw the light of the day',
  2008: 'Development of Pixal continues eating my life, mostly.',
  2009: 'The first version of Expedition appears',
  2010: 'Pixal development ends. Slashie pushes ahead with the graphical version of Expedition.',
  2011: 'Slashware 1 is created as slashie dreams of indiedev greatness with Expedition 1, but takes a turn towards business dev instead.',
  2012: 'Slashware 1 drifts looking for an identity with many unfocused projects including a remake of ArcherFire made in Flash. Expedition 1 dies.',
  2013: 'Slashware 1 tries to survive but fails to make his way through the golden age of Desktop social media freemium.',
  2014: 'The renaissance of slashie as indiedev starts slow, with a first version of Ananias.',
  2015: 'Slashie continues building experience in procedural content generation, as the development of Ananias continues.',
  2016: 'As Ananias continues development, small experiments are made. First visit to San Francisco for Roguelike Celebration.',
  2017: 'A second age of JavaScript roguelikes starts.',
  2018: 'Slashware rises from the ashes, a new 3D incarnation of "Expedition" is the new hope.',
  2019: 'Expedition is further developed as it wins government funds',
  2020: 'As civilization collapses due to a deadly virus, Emerald Woods is born and Expedition transforms into NovaMundi.',
  2021: 'Development of NovaMundi is pushed forward.',
  2022: 'A procjam entry derails slashie into FormulaProc while NovaMundi development struggles.',
  2023: 'Emerald Woods has an unexpected revival while Slashie tried again to make NovaMundi\'s full release.',
  2024: 'Old projects get surprise releases while Slashie finally manages to do NovaMundi\'s full release.',
  2025: 'What is slashie\'s next project? Maybe Emerald Woods will get a bit push this year.'
}

const yearsImages = {
  1995: 'img/years/1995.jpg',
  2001: 'img/years/2001.jpg',
  2003: 'img/years/2003.jpg',
  2004: 'img/years/2004.jpg',
  2005: 'img/years/2005.jpg',
  2006: 'img/years/2006.jpg',
  2007: 'img/years/2007.jpg',
  2008: 'img/years/2008.jpg',
  2009: 'img/years/2009.jpg',
  2010: 'img/years/2010.jpg',
  2011: 'img/years/2011.jpg',
  2012: 'img/years/2012.jpg',
  2013: 'img/years/2013.jpg',
  2014: 'img/years/2014.jpg',
  2015: 'img/years/2015.jpg',
  2016: 'img/years/2016.jpg',
  2017: 'img/years/2017.jpg',
  2018: 'img/years/2018.jpg',
  2019: 'img/years/2019.jpg',
  2020: 'img/years/2020.jpg',
  2021: 'img/years/2021.jpg',
  2022: 'img/years/2022.jpg',
  2023: 'img/years/2023.jpg',
  2024: 'img/years/2024.jpg',
  2025: 'img/years/2025.jpg',
}

class ProjectsArea extends Component {
  render() {
    const normalGroups = this.props.groups.filter(g => !g.skipGrouping);
    const staticGroups = this.props.groups.filter(g => g.skipGrouping);
    const groups = this.yearRehash(normalGroups).concat(staticGroups);
    return (
      <div>
      {groups.map((group) => (<ProjectsGroup group={group}/>))}
      </div>
    )
  }

  // Create new groups by year
  yearRehash (groups) {
    let allProjects = [];
    let minYear = -1, maxYear = 9999;
    groups.forEach(group => {
      group.projects.forEach(project => {
        project.group = group;
        if (project.year > maxYear) {
          maxYear = project.year;
        }
        if (project.year < minYear) {
          minYear = project.year;
        }
      });
      allProjects = allProjects.concat(group.projects);
    });
    allProjects.sort(function(a,b){
      //return (a.title<b.title?-1:(a.title>b.title?1:0)); 
      const bYear = b.lastUpdate || b.year;
      const aYear = a.lastUpdate || a.year;
      return bYear - aYear;
    });
    const yearGroups = [];
    for (let year = maxYear; year >= minYear; year--) {
      const group = {
        name: year.toString(),
        text: yearsText[year],
        projects: allProjects.filter(p => p.year === year),
        continuedProjects: allProjects.filter(p=> p.years && p.year !== year && p.years.indexOf(year) !== -1),
        image: yearsImages[year]
      };
      yearGroups.push(group);
    }
    return yearGroups;

  }
}


class ProjectsGroup extends Component {
  render() {
    const group = this.props.group;
    const projects = group.projects.filter(p => !p.client);
    // Sort alpha
    projects.sort(function(a,b){
      //return (a.title<b.title?-1:(a.title>b.title?1:0)); 
      const bYear = b.lastUpdate || b.year;
      const aYear = a.lastUpdate || a.year;
      return bYear - aYear;
    });

    const hasImage = group.image !== undefined;
    if (hasImage){
      projects.unshift({
        special: 'pic',
        image: group.image,
        text: group.text
      })
    }

    let hasContinuedProjects = false;
    if (group.continuedProjects && group.continuedProjects.length > 0) {
      projects.push({
        special: 'summary',
        text: 'Also worked on: ' + group.continuedProjects.map(p => p.title).join(', ') + '.'
      })
      hasContinuedProjects = true;
    }

    // Split array in groups of 3
    const projectGroups = [];
    let currentProjectGroup = -1;
    projects.map((project, index) => {
      if (index%3 === 0){
        currentProjectGroup++;
        projectGroups[currentProjectGroup] = [];
      }
      projectGroups[currentProjectGroup].push(project);
      return false; 
    });
    const noSummary = this.props.noSummary || group.skipGrouping;
    if (projects.length === 0){
      return null;
    }
    let numProjects = hasImage ? projects.length - 1 : projects.length;
    if (hasContinuedProjects) {
      numProjects--;
    }
    return (
      <div className="container">
        <h2>{group.name} {(noSummary || numProjects === 0) ? "" : "("+numProjects+" projects)"}</h2>
        <p>{hasImage ? "" : group.text}</p>
        <div className="container">
          {projectGroups.map((projectGroup, index) => {
            return <ProjectsRow projects={projectGroup} key={index}/>
          })}
        </div>
      </div>
    )
  }
}

function ProjectsRow(props){
  return (
    <div className="row">
      {props.projects.map((project, index) => {
        return (
          <ProjectEntry key={index} project={project}/>
        )
      })}
    </div>
  )
}

export default ProjectsArea;