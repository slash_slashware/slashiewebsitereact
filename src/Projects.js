import axios from 'axios';
import randomTweetGen from './randomTweet';

let Projects = {
	projectsData: [],
  randomTweet: '', 
  load: function() {
    return axios.get("data/projects.json")
      .then((result) => {    
        this.projectsData = result.data.projects;
        this.randomTweet = randomTweetGen(this.projectsData);
        const projectMap = {};
        this.projectsData.forEach((group) => {
          group.projects.forEach((project) => {
            if (project.collaborators) project.collaborators.forEach((collaborator) => {
              if (!projectMap[collaborator]) {
                projectMap[collaborator] = [];
              }
              projectMap[collaborator].push(project.title);
            });
          });
        });
        const collaboratorsSection = this.projectsData.find(p => p.id === 'collaborators');
        collaboratorsSection.projects.forEach(p => {
          if (projectMap[p.key]){
            p.projects = projectMap[p.key];
          } else {
            p.projects = [];
          }
        });
        collaboratorsSection.projects = collaboratorsSection.projects.sort((a,b) => b.projects.length - a.projects.length);
      });
  },
	all: function(){
		return this.projectsData;
	},
	filter: function(filter){
		if (!filter){
			return this.all();
		}
		const filteredGroups = this.projectsData.map((group, index) => {
			let filteredGroup = Object.assign({}, group);
			filteredGroup.projects = this.projectsData[index].projects.filter((project) => {
				if (filter.type === "status"){
					return project.status === filter.value;
        } else if (filter.type === "activity"){
          return project.activity === filter.value
				} else {
					return false;
				}
			});
			return filteredGroup;
		})
		console.log(filteredGroups);
		return filteredGroups;
	}
}

export default Projects