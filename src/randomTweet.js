const ss = require('seededshuffle');

const MODE_RANDOM = false;

function getTweet(project) {
	const activeDevelopment = project.lastUpdate ? project.year + ' to ' + project.lastUpdate : project.year;
	let collabs = '';
	if (project.collaborators && project.collaborators.length > 0) {
		collabs = ' Made with ';
		project.collaborators.forEach((c,i) => {
			if (i == project.collaborators.length - 1) {
				if (i > 0)
					collabs += ' and ' + c + '.';
				else
					collabs += c + '.';
			} else {
				if (i > 0) {
					collabs += ', ' + c;
				} else {
					collabs += c;
				}
					
			}
		});
	}

	const hashtagize = s => '#' + s;

	const hashtags = (project.genre || []).map(g => hashtagize(g)).concat((project.technologies || []).map(g => hashtagize(g))).concat((project.artStyle || []).map(g => hashtagize(g))).reduce((accumulator, currentValue) => accumulator + ' ' + currentValue, '');

	let link = '';
	if (project.buttons && project.buttons.length > 0) {
		// Look for a public URL
		project.buttons.forEach(b => {
			if (b.url.indexOf('http') === 0) {
				link = b.url;
			}
		});
	} 

	const text = project.title + ' (' + activeDevelopment + ', ' + project.status + '), ' + project.text + ' Dev ' + project.activity + '.' + collabs + hashtags + ' #indiedev ';
	return {
		text,
		image: 'https://slashie.net/' + project.image,
		link
	}
}

module.exports = function (projects) {
	let flatProjects = [];

	projects.forEach(projectGroup => {
		const simpleProjectGroup = Object.assign({}, projectGroup);
		delete simpleProjectGroup.projects;
		if (projectGroup.skipGrouping) {
			return;
		}
		flatProjects = flatProjects.concat(projectGroup.projects.map(project => {
			project.group = simpleProjectGroup;
			return project;
		}));
	});
	let project;
	if (MODE_RANDOM === true) {
		project = flatProjects[Math.floor(Math.random() * flatProjects.length - 1)];
	} else {
		flatProjects = ss.shuffle(flatProjects, 'patienceIsTheKey', true);
		const now = new Date();
		const fullDaysSinceEpoch = Math.floor(now/8.64e7);
		project = flatProjects[fullDaysSinceEpoch % flatProjects.length];
	}
	return getTweet(project);
}