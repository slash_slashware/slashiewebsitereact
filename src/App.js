import React, { Component } from 'react';
import './slashie.css';
import Projects from './Projects';
import FiltersBar from './FiltersBar.class'
import ProjectsArea from './ProjectsArea.class'

class App extends Component {
  constructor(){
    super();
    this.state = {
      displayedGroups: []
    }
    Projects.load().then(() => {
      this.setState({ displayedGroups: Projects.all() });
    });
  }

  changeFilter(filter){
    this.setState(
       {displayedGroups: Projects.filter(filter)}
    );
  }

  render() {
    return (
      <div>
        <Header filterFunction={this.changeFilter.bind(this)}/>
        <ProjectsArea groups={this.state.displayedGroups}/>
        <Footer/>
      </div>
    );
  }
}

class Header extends Component {
  // Filters have been disabled: <FiltersBar onClick={this.props.filterFunction}/>
  render() {
    return (
      <section className="container">
        <section className="page-header">
          <div className="row">
            <div className="col-md-2">
              <div id="logoImage"></div>
            </div>
            <div className="col-md-6">
              <h3>Welcome...</h3>
              <p>I am Santiago Zapata (a.k.a. slashie), a videogame developer.</p>
              <p>Here you will find a collection of things I've made, alone or with friends, which include roguelikes, procedural content generation, exploration games, online environments, homages to classic series as genre-mashups, creativity platforms, collection games, 
                space shooters, educational games, point-and-click adventures, strategy/tactics and more!</p>
              <p>I like gamejams.</p>
              <p>This is my personal projects collection, client work is normally not included unless I had a critical role on the inception of the project.</p>
              <ul className="nav nav-pills">
                <li><a href="https://blog.slashie.net" target="_blank">Blog</a></li>
                <li><a href="https://www.youtube.com/@slashie" target="_blank">YouTube</a></li>
                <li><a href="https://twitter.com/slashie_" target="_blank">Twitter</a></li>
                <li><a href="https://bsky.app/profile/slashie.bsky.social" target="_blank">Bsky</a></li>
                <li><a href="https://linkedin.com/in/slashie/" target="_blank">LinkedIn</a></li>
              </ul>
              <p>I'm also the founder of <a href = "http://slashware.games" target = "_blank">Slashware Interactive</a>, head there for my published projects.</p>
            </div>
            <section id = "randomTweet" className="col-md-4">
              <p><b>Game of the Day</b></p>
              <div>
                <div className="thumbnail">
                  <img src = {Projects.randomTweet.image}/>
                  <div className="caption">
                    {Projects.randomTweet.text} <a href = {Projects.randomTweet.link}>{Projects.randomTweet.link}</a>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>
      </section>
    )
  }
}

function Footer(props){
  let currentYear = new Date().getFullYear();
  return (
    <div id="footer" className="text-right">
      <p>Copyright (c) 2004 - {currentYear} Santiago Zapata</p>
      <p>Generated using ReactJS, Credits: <a href = "iconLicenses">Icon licenses</a></p>
    </div>
  );
}

export default App;
