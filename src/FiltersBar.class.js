import React, { Component } from 'react';

const FILTERS = [
	{
		type: "status",
		value: "public"
	},
	{
		type: "status",
		value: "beta"
	},
		{
		type: "status",
		value: "alpha"
	},
	{
		type: "status",
		value: "pre-alpha"
	},
	{
		type: "status",
		value: "idea"
	},
	{
		type: "activity",
		value: "active"
	},
		{
		type: "activity",
		value: "finished"
	},
	{
		type: "activity",
		value: "paused"
	},
	{
		type: "activity",
		value: "stalled"
	},
	{
		type: "activity",
		value: "stopped"
	}
];

class FiltersBar extends Component {
	handleClick(filter){
		this.onClick(filter)
	}

	render(){
		this.onClick = this.props.onClick.bind(this);
		return (
			<section>
				Filter by <b>Completion Status</b>: 
				<ul className="nav nav-pills">
					{FILTERS.filter(f => f.type === 'status').map((filter) => {
						return (
							<li>
								<FilterButton filter={filter} onClick={(ev) => this.handleClick(filter)}/>
							</li>
						)
					})}
				</ul>
				Filter by Project Activity: 
				<ul className="nav nav-pills">
					{FILTERS.filter(f => f.type === 'activity').map((filter) => {
						return (
							<li>
								<FilterButton filter={filter} onClick={(ev) => this.handleClick(filter)}/>
							</li>
						)
					})}
				</ul>
				<FilterButton all="all" onClick={(ev) => this.handleClick(null)}/>
			</section>
		);
		
	}	
}

function FilterButton(props){
	if (props.all){
		return (<a href="#" onClick={props.onClick}>Remove filters</a>)
	} else {
		return (<a href="#" onClick={props.onClick}>{props.filter.value}</a>)
	}
}

export default FiltersBar;