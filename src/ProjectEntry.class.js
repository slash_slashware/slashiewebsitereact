import React, { Component } from 'react';

class ProjectEntry extends Component {
  render(){
    let project = this.props.project;
    const hasImage = project.image !== undefined;
    let image = null;
    if (hasImage){
      if (project.url) {
        image = <a href={project.url} target="_blank"><img className="cardImage" src={"/"+project.image} alt={"Picture of "+project.title}/></a>;
      } else {
        image = <img className="cardImage" src={"/"+project.image} alt={"Picture of "+project.title}/>;
      }
    }

    if (!project.buttons){
      project.buttons = [];
    }

    let tags = [];

    tags.push({
      type: 'status',
      name: project.status
    })
    tags.push({
      type: 'activity',
      name: project.activity
    })
    if (project.genre) tags = tags.concat(project.genre.map((genre) => {return {type: 'genre', name: genre};}));
    if (project.collaborators) tags = tags.concat(project.collaborators.map((collaborator) => {return {type: 'collaborator', name: collaborator};}));
    if (project.technologies) tags = tags.concat(project.technologies.map((technology) => {return {type: 'technology', name: technology};}));
    if (project.artStyle) tags = tags.concat(project.artStyle.map((artStyle) => {return {type: 'artStyle', name: artStyle};}));
    if (project.projects) tags = tags.concat(project.projects.map((project) => {return {type: 'project', name: project};}));

    let pixelArtIcon = null;
    if (project.skills && project.skills.indexOf("pixel-art") != -1) {
      pixelArtIcon = <img src={"/img/icons/pixelArtist.png"} alt="Pixel Artist" title = "Pixel Artist" className="skillIcon"/>;
    }
    let codeIcon = null;
    if (project.skills && project.skills.indexOf("code") != -1) {
      codeIcon = <img src={"/img/icons/code.png"} alt="Coder" title = "Coder" className="skillIcon"/>;
    }
    let musicIcon = null;
    if (project.skills && project.skills.indexOf("music") != -1) {
      musicIcon = <img src={"/img/icons/music.png"} alt="Musician" title ="Musician" className="skillIcon"/>;
    }
    if (project.skills && project.skills.indexOf("sound") != -1) {
      musicIcon = <img src={"/img/icons/music.png"} alt="Sound" title ="Sound" className="skillIcon"/>;
    }
    let artIcon = null;
    if (project.skills && project.skills.indexOf("illustration") != -1) {
      artIcon = <img src={"/img/icons/artist.png"} alt="Illustrator" title = "Illustrator" className="skillIcon"/>;
    }
    let modelingIcon = null;
    if (project.skills && project.skills.indexOf("3d") != -1) {
      modelingIcon = <img src={"/img/icons/3d.png"} alt="3D Modeling" title = "3D Modeling" className="skillIcon"/>;
    }
    let writerIcon = null;
    if (project.skills && project.skills.indexOf("writer") != -1) {
      writerIcon = <img src={"/img/icons/writer.png"} alt="Writer/Research" title = "Writer/Research" className="skillIcon"/>;
    }

    let countryIcon = null;
    if (project.country) {
      countryIcon = <img src={"/img/icons/country-" + project.country + ".png"} alt = {project.country} title = {project.country} className="countryIcon"/>;
    }

    const origin = project.origin ? <p>{project.origin}</p> : null;

    const year = project.lastUpdate  && project.lastUpdate != project.year ?
      <p>{'Last update on ' + project.lastUpdate}</p> : null;
    return (
      <div className="col-md-4">
          <div className="thumbnail">
            {image}
            <div className="caption">
              <h3>{project.title}{countryIcon}{pixelArtIcon}{artIcon}{modelingIcon}{musicIcon}{codeIcon}{writerIcon}</h3>
              {origin}
              {year}
              <p>
                {tags.map((tag, index) => 
                  <Tag tag={tag} key={index}/>
                )}
              </p>
              {project.special === 'summary' ? (
                <p><i>{project.text}</i></p>
              ) : (
                <p>{project.text}</p>
              )}
              {project.buttons.map((button, index) => (
                <a key={index} href={button.url} target="_blank" className="btn btn-primary" role="button">{button.title}</a>
              ))}
            </div>
          </div>
        </div>
    )
  }
} 

const tagClasses = {
  status: "label-success",
  project: "label-success",
  activity: "label-danger",
  genre: "label-primary",
  collaborator: "label-default",
  technology: "label-warning",
  artStyle: "label-danger"
}

class Tag extends Component {
  render(){
    let tag = this.props.tag;
    let tagClass = tagClasses[tag.type];
    return (
      <span className={"spaciousLabel label "+tagClass}>{tag.name}</span>
    )
  }
}

export default ProjectEntry